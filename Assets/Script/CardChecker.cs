using UnityEngine;
using UnityEditor;
using System.Collections;

public class CardChecker : MonoBehaviour {

	private int numberOfCard;
	//private string[] cardSuitRank;
	public int[] diamond;
	public int[] club;
	public int[] heart;
	public int[] spade;

	private bool isInvalid;
	private bool isHigher;
	private bool isPair;
	private bool isThrees;
	private bool isFullHouse;
	public bool isStraight;
	public bool isFlush;
	public bool isStraightFlush;

	private GameObject sortTemp;
	private GameObject gameManager;

	private string cardType;
	private int cardSuit;
	private int cardRank;
	
	// Use this for initialization
	void Start () {
		gameManager = GameObject.Find("gameManager");
		numberOfCard = 0;
		diamond = new int[0];
		club = new int[0];
		heart = new int[0];
		spade = new int[0];
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void cardCalculation(GameObject[] selectedCard){
		//this method used for check card that send to game manager. will check card type, rank, and suit
		if(selectedCard.Length!=0){
			isInvalid=false;
			isHigher=false;
			isPair=false;
			isThrees=false;
			isFullHouse = false;
			isStraight=false;
			isFlush=false;
			isStraightFlush=false;

			cardType = null;
			cardSuit = 0;
			cardRank = 0;
		
			//**********************************
			//*Rank Sorting                    *
			//**********************************
			for(int i=0;i<selectedCard.Length;i++){
				for(int j=0;j<selectedCard.Length;j++){
					if(selectedCard[i].gameObject.GetComponent<Card>().suit < 
					   selectedCard[j].gameObject.GetComponent<Card>().suit){
						sortTemp = selectedCard[i];
						selectedCard[i]=selectedCard[j];
						selectedCard[j]=sortTemp;
					}
					if(selectedCard[i].gameObject.GetComponent<Card>().rank < 
					   selectedCard[j].gameObject.GetComponent<Card>().rank){
						sortTemp = selectedCard[i];
						selectedCard[i]=selectedCard[j];
						selectedCard[j]=sortTemp;
					}
				}
			}

			if(selectedCard.Length==1 && (gameManager.GetComponent<GameManager>().highestType=="High" ||
			                              gameManager.GetComponent<GameManager>().highestType=="")){

				cardRank=selectedCard[0].GetComponent<Card>().rank;
				cardSuit=selectedCard[0].GetComponent<Card>().suit;
				cardType="High";
				isHigher=true;

			}else if(selectedCard.Length==2 && selectedCard[0].GetComponent<Card>().rank ==selectedCard[1].GetComponent<Card>().rank &&
			         (gameManager.GetComponent<GameManager>().highestType=="Pair" ||
					 gameManager.GetComponent<GameManager>().highestType=="")){

				cardRank=selectedCard[0].GetComponent<Card>().rank;
				cardSuit=selectedCard[1].GetComponent<Card>().suit; // higher index is higher suit level
				cardType="Pair";
				isPair=true;

			}else if(selectedCard.Length==3 && 
			         selectedCard[0].GetComponent<Card>().rank ==selectedCard[1].GetComponent<Card>().rank &&
			         selectedCard[0].GetComponent<Card>().rank ==selectedCard[2].GetComponent<Card>().rank &&
			         (gameManager.GetComponent<GameManager>().highestType=="Threes" ||
					 gameManager.GetComponent<GameManager>().highestType=="")){

				cardRank=selectedCard[0].GetComponent<Card>().rank;
				cardSuit=selectedCard[1].GetComponent<Card>().suit; // higher index is higher suit level
				cardType="Threes";
				isThrees=true;
			
			}else if(selectedCard.Length==5){
				if((selectedCard[1].GetComponent<Card>().rank == selectedCard[0].GetComponent<Card>().rank+1) &&
				   selectedCard[2].GetComponent<Card>().rank == (selectedCard[0].GetComponent<Card>().rank)+2 &&
				   selectedCard[3].GetComponent<Card>().rank == (selectedCard[0].GetComponent<Card>().rank)+3 &&
				   selectedCard[4].GetComponent<Card>().rank == (selectedCard[0].GetComponent<Card>().rank)+4 &&
				   (gameManager.GetComponent<GameManager>().highestType=="Straight" ||
				 	gameManager.GetComponent<GameManager>().highestType==""))

				{
					cardRank=selectedCard[4].GetComponent<Card>().rank;
					cardSuit=selectedCard[4].GetComponent<Card>().suit;
					cardType="Straight";
					isStraight=true;

				}else if(selectedCard[1].GetComponent<Card>().rank ==selectedCard[0].GetComponent<Card>().rank &&
			         	selectedCard[2].GetComponent<Card>().rank ==selectedCard[0].GetComponent<Card>().rank &&
			         	selectedCard[4].GetComponent<Card>().rank ==selectedCard[3].GetComponent<Card>().rank &&
			        	(gameManager.GetComponent<GameManager>().highestType=="Full House" ||
				 		gameManager.GetComponent<GameManager>().highestType=="Flush" ||
			 			gameManager.GetComponent<GameManager>().highestType=="Straight" ||
			 			gameManager.GetComponent<GameManager>().highestType=="")){

					cardRank=selectedCard[2].GetComponent<Card>().rank;
					cardSuit=selectedCard[2].GetComponent<Card>().suit;
					cardType="FullHouse";
					isFullHouse=true;

				}else if(selectedCard[1].GetComponent<Card>().rank ==selectedCard[0].GetComponent<Card>().rank &&
						selectedCard[3].GetComponent<Card>().rank ==selectedCard[2].GetComponent<Card>().rank &&
						selectedCard[4].GetComponent<Card>().rank ==selectedCard[2].GetComponent<Card>().rank &&
			        	(gameManager.GetComponent<GameManager>().highestType=="Full House" ||
				 		gameManager.GetComponent<GameManager>().highestType=="Flush" ||
						gameManager.GetComponent<GameManager>().highestType=="Straight" ||
				 		gameManager.GetComponent<GameManager>().highestType=="")){

					cardRank=selectedCard[4].GetComponent<Card>().rank;
					cardSuit=selectedCard[4].GetComponent<Card>().suit;
					cardType="FullHouse";
					isFullHouse=true;

				}
				//**********************************
				//*Rank Sorting End                *
				//**********************************
				

				//**********************************
				//*Suit Sorting                    *
				//**********************************
				for(int i=0;i<selectedCard.Length;i++){
					for(int j=0;j<selectedCard.Length;j++){
						if(selectedCard[i].gameObject.GetComponent<Card>().cardIndex < 
						   selectedCard[j].gameObject.GetComponent<Card>().cardIndex){
							sortTemp = selectedCard[i];
							selectedCard[i]=selectedCard[j];
							selectedCard[j]=sortTemp;
						}
					}
				}
				//card classification
				ArrayUtility.Clear(ref diamond);
				ArrayUtility.Clear(ref club);
				ArrayUtility.Clear(ref heart);
				ArrayUtility.Clear(ref spade);

				for(int i=0;i<selectedCard.Length;i++){
					switch(selectedCard[i].gameObject.GetComponent<Card>().suit){
						case 0:ArrayUtility.Add(ref diamond, selectedCard[i].gameObject.GetComponent<Card>().rank);
							break;
						case 1:ArrayUtility.Add(ref club, selectedCard[i].gameObject.GetComponent<Card>().rank);
							break;
						case 2:ArrayUtility.Add(ref heart, selectedCard[i].gameObject.GetComponent<Card>().rank);
							break;
						case 3:ArrayUtility.Add(ref spade, selectedCard[i].gameObject.GetComponent<Card>().rank);
							break;
					}
				}

				if((diamond.Length == 5 || club.Length == 5 || heart.Length == 5 || spade.Length == 5) &&
				   (gameManager.GetComponent<GameManager>().highestType=="Flush" ||
				 	gameManager.GetComponent<GameManager>().highestType=="Straight" ||
				 	gameManager.GetComponent<GameManager>().highestType=="")){
						cardRank=selectedCard[4].GetComponent<Card>().rank;
						cardSuit=selectedCard[4].GetComponent<Card>().suit; 
						isFlush=true;
						cardType="Flush";
				}

				//**********************************
				//*Suit Sorting End                *
				//**********************************

				if((selectedCard[1].GetComponent<Card>().rank == selectedCard[0].GetComponent<Card>().rank+1) &&
				   selectedCard[2].GetComponent<Card>().rank == (selectedCard[0].GetComponent<Card>().rank)+2 &&
				   selectedCard[3].GetComponent<Card>().rank == (selectedCard[0].GetComponent<Card>().rank)+3 &&
				   selectedCard[4].GetComponent<Card>().rank == (selectedCard[0].GetComponent<Card>().rank)+4 &&
				   (gameManager.GetComponent<GameManager>().highestType=="Flush" ||
				   	gameManager.GetComponent<GameManager>().highestType=="Full House" ||
				   	gameManager.GetComponent<GameManager>().highestType=="Straight" ||
				 	gameManager.GetComponent<GameManager>().highestType=="Straight Flush" ||
					gameManager.GetComponent<GameManager>().highestType=="")){
					cardRank=selectedCard[4].GetComponent<Card>().rank;
					cardSuit=selectedCard[4].GetComponent<Card>().suit; 
					isStraightFlush=true;
					cardType="Straight Flush";
				}
			}

			if(isHigher || isPair || isThrees || isFullHouse || isStraight || isFlush || isStraightFlush ){
				if(gameManager.GetComponent<GameManager>().highestRank < cardRank){  //check if the card Rank bigger than current card Rank
					sendCard(selectedCard);
					if(cardType=="Flush" || cardType=="Straight Flush"){
							ArrayUtility.Clear(ref diamond);
							ArrayUtility.Clear(ref club);
							ArrayUtility.Clear(ref heart);
							ArrayUtility.Clear(ref spade);
					}
					gameManager.GetComponent<GameManager>().highestType=cardType;
					gameManager.GetComponent<GameManager>().highestSuit=cardSuit;
					gameManager.GetComponent<GameManager>().highestRank=cardRank;	
				}else if(gameManager.GetComponent<GameManager>().highestRank == cardRank &&
				         gameManager.GetComponent<GameManager>().highestSuit <= cardSuit){ //check if the card have same suit and the card rank bigger than current card
					sendCard(selectedCard);
					gameManager.GetComponent<GameManager>().highestType=cardType;
					gameManager.GetComponent<GameManager>().highestSuit=cardSuit;
					gameManager.GetComponent<GameManager>().highestRank=cardRank;
				}
			}else{
				Debug.Log("Invalid");
			}

			if(!isInvalid){

			}
		}
	}

	void sendCard(GameObject[] selectedCard){
		//send the selection card to beat current player's card
		if(GameObject.Find("gameManager").GetComponent<GameManager>().selectedCardOnTable.Length!=0){
			for(int i=0;i<GameObject.Find("gameManager").GetComponent<GameManager>().selectedCardOnTable.Length;i++){
				GameObject.Find("gameManager").GetComponent<GameManager>().selectedCardOnTable[i].SetActive(false);
			}
		}

		ArrayUtility.Clear(ref GameObject.Find("gameManager").GetComponent<GameManager>().selectedCardOnTable);
		for(int i=0;i<selectedCard.Length;i++){
			//selectedCard[i].SetActive(false);
			iTween.MoveTo(selectedCard[i],new Vector3(i+1,0,0),1);
			iTween.MoveTo(GameObject.Find("Deck"+GameObject.Find("gameManager").GetComponent<GameManager>().playerTurn), 
			              new Vector3(GameObject.Find("Deck"+GameObject.Find("gameManager").GetComponent<GameManager>().playerTurn).GetComponent<Transform>().position.x,
			            				GameObject.Find("Deck"+GameObject.Find("gameManager").GetComponent<GameManager>().playerTurn).GetComponent<Transform>().position.y-3,
			           					GameObject.Find("Deck"+GameObject.Find("gameManager").GetComponent<GameManager>().playerTurn).GetComponent<Transform>().position.z)
			              ,2);
			GameObject.Find("Deck"+GameObject.Find("gameManager").GetComponent<GameManager>().playerTurn).GetComponent<Deck>().cardNumber--;
			ArrayUtility.Add(ref GameObject.Find("gameManager").GetComponent<GameManager>().selectedCardOnTable,selectedCard[i]);
		}

		GameObject.Find("gameManager").GetComponent<GameManager>().highestPlayer=GameObject.Find("gameManager").GetComponent<GameManager>().playerTurn;

		if(GameObject.Find("gameManager").GetComponent<GameManager>().playerTurn!=4){
			GameObject.Find("gameManager").GetComponent<GameManager>().playerTurn++;
		}else{
			GameObject.Find("gameManager").GetComponent<GameManager>().playerTurn=1;
		}

		ArrayUtility.Clear(ref GameObject.Find("gameManager").GetComponent<GameManager>().selectedCard);
	}
}
