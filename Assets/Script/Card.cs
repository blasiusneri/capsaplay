﻿using UnityEngine;
using System.Collections;

public class Card : MonoBehaviour {
	
	public int cardIndex;
	public int suit;
	public int rank;
	public int inHandIndex; //marking the card belong to a player
	
}
