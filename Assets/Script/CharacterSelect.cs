﻿using UnityEngine;
using System.Collections;

public class CharacterSelect : MonoBehaviour {

	private int buttonWidth = 180;
	private int buttonHeight = 180;
	public Texture charA;
	public Texture charB;
	public static int character;

	void Awake(){

	}
	
	void OnGUI(){
		if(GUI.Button(new Rect((Screen.width/2)-(buttonWidth),(Screen.height/2)-(buttonHeight/2),buttonWidth,buttonHeight),charA)){
			character = 1;
			Application.LoadLevel(2);
		}
		
		if(GUI.Button(new Rect((Screen.width/2)+(buttonWidth/2),(Screen.height/2)-(buttonHeight/2),buttonWidth,buttonHeight),charB)){
			character = 2;
			Application.LoadLevel(2);
		}
	}
}
