﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class GameManager : MonoBehaviour {

	public GameObject playerPrefab;
	public GameObject[] selectedCardOnTable;
	public GameObject[] selectedCard;
	public static GameManager instance = null;
	public static GameObject[] enemy;
	private int index;
	
	public string highestType = "";
	public int highestSuit = 0;
	public int highestRank = 0;
	public int highestPlayer;

	public int playerTurn;
	public int[] skipTurn;
	public bool deckCalled;
	public bool isGameOver;

	public GameObject avatarPrefabA;
	public GameObject avatarPrefabB;
	private GameObject displayAvatar;
	public int playerIndex;
	private Sprite[] avaSpriteA;
	private Sprite[] avaSpriteB;

	public int meanNumbCard;

	void Awake(){
		if(instance==null)
			instance = this;
		else if(instance!=this)
			Destroy(gameObject);
		selectedCardOnTable = new GameObject[0];
		selectedCard = new GameObject[0];
		playerTurn=1;
		deckCalled=false;
		isGameOver = false;
		avaSpriteA = Resources.LoadAll<Sprite>("CharA");
		avaSpriteB = Resources.LoadAll<Sprite>("CharB");
		InitGame();
	}

	void Start(){
		if(CharacterSelect.character==1){
			displayAvatar = Instantiate(avatarPrefabA,new Vector3(0,0,0),Quaternion.identity) as GameObject;
			displayAvatar.GetComponent<SpriteRenderer>().sprite = avaSpriteA[0];
			displayAvatar.transform.position = new Vector3(7,3,0);
		}else if(CharacterSelect.character==2){
			displayAvatar = Instantiate(avatarPrefabB,new Vector3(0,0,0),Quaternion.identity) as GameObject;
			displayAvatar.GetComponent<SpriteRenderer>().sprite = avaSpriteB[0];
			displayAvatar.transform.position = new Vector3(7,3,0);
		}
	}

	void InitGame(){
		skipTurn = new int[0];
	}

	void Update(){
		if(skipTurn.Length==3){
			highestType = "";
			highestSuit = 0;
			highestRank = 0;
			ArrayUtility.Clear(ref skipTurn);

			if(GameObject.Find("Deck1").GetComponent<Deck>().cardNumber==0 ||
			   GameObject.Find("Deck2").GetComponent<Deck>().cardNumber==0 ||
			   GameObject.Find("Deck3").GetComponent<Deck>().cardNumber==0 ||
			   GameObject.Find("Deck4").GetComponent<Deck>().cardNumber==0){
				isGameOver=true;
			}
		}

		//player1 will change depend comparison with mean of three other player's card
		meanNumbCard=(GameObject.Find("Deck2").GetComponent<Deck>().cardNumber+
						GameObject.Find("Deck3").GetComponent<Deck>().cardNumber+
		              	GameObject.Find("Deck4").GetComponent<Deck>().cardNumber)/3;

		if(GameObject.Find("Deck1").GetComponent<Deck>().cardNumber<meanNumbCard){
			if(CharacterSelect.character==1)
				displayAvatar.GetComponent<SpriteRenderer>().sprite = avaSpriteA[1];
			else
				displayAvatar.GetComponent<SpriteRenderer>().sprite = avaSpriteB[1];
		}else if(GameObject.Find("Deck1").GetComponent<Deck>().cardNumber == meanNumbCard){
			if(CharacterSelect.character==1)
				displayAvatar.GetComponent<SpriteRenderer>().sprite = avaSpriteA[0];
			else
				displayAvatar.GetComponent<SpriteRenderer>().sprite = avaSpriteB[0];
		}else if(GameObject.Find("Deck1").GetComponent<Deck>().cardNumber>meanNumbCard){
			if(CharacterSelect.character==1)
				displayAvatar.GetComponent<SpriteRenderer>().sprite = avaSpriteA[2];
			else
				displayAvatar.GetComponent<SpriteRenderer>().sprite = avaSpriteB[2];
		}

		if(Application.platform == RuntimePlatform.OSXDashboardPlayer ||     //make sure this mouse down button work in either mac or pc
		   Application.platform == RuntimePlatform.OSXEditor ||
		   Application.platform == RuntimePlatform.OSXPlayer ||
		   Application.platform == RuntimePlatform.OSXWebPlayer ||
		   Application.platform == RuntimePlatform.WindowsEditor ||
		   Application.platform == RuntimePlatform.WindowsPlayer ||
		   Application.platform == RuntimePlatform.WindowsWebPlayer)
		{
			if(Input.GetMouseButtonDown(0))
			{
				Ray screenRay = Camera.main.ScreenPointToRay(Input.mousePosition);
				RaycastHit2D hit = Physics2D.Raycast(screenRay.origin, screenRay.direction);
				if(hit){
					if(hit.collider.gameObject.CompareTag("Card")){
						index = hit.collider.gameObject.GetComponent<Card>().cardIndex;
						int cardIndex = 0;
						for(int i=0;i<GameObject.Find("cardManager").GetComponent<CardManager>().cards.Length;i++){
							if(index==GameObject.Find("cardManager").GetComponent<CardManager>().cards[i].GetComponent<Card>().cardIndex){
								cardIndex=i;
							}
						}

						if(!ArrayUtility.Contains(selectedCard,GameObject.Find("cardManager").GetComponent<CardManager>().cards[cardIndex]) &&
						   (GameObject.Find("cardManager").GetComponent<CardManager>().cards[cardIndex].GetComponent<Card>().inHandIndex == playerTurn) &&
						   selectedCard.Length<5){  //this block use for select card from player hand
							ArrayUtility.Add(ref selectedCard,GameObject.Find("cardManager").GetComponent<CardManager>().cards[cardIndex]);
							hit.collider.gameObject.transform.position=new Vector3(
								hit.collider.gameObject.transform.position.x,
								hit.collider.gameObject.transform.position.y+1,
								hit.collider.gameObject.transform.position.z);
						}else if(ArrayUtility.Contains(selectedCard,GameObject.Find("cardManager").GetComponent<CardManager>().cards[cardIndex]) && 
						         (GameObject.Find("cardManager").GetComponent<CardManager>().cards[cardIndex].GetComponent<Card>().inHandIndex == playerTurn) &&
						         selectedCard.Length>0){ //this block use for deselect card from player hand
							ArrayUtility.Remove(ref selectedCard,GameObject.Find("cardManager").GetComponent<CardManager>().cards[cardIndex]);
							hit.collider.gameObject.transform.position=new Vector3(
								hit.collider.gameObject.transform.position.x,
								hit.collider.gameObject.transform.position.y-1,
								hit.collider.gameObject.transform.position.z);
						}
					}
				}
			}
		}
	}

	void onPlayerTurn(int i){
		iTween.MoveTo(GameObject.Find("Deck"+i), new Vector3(GameObject.Find("Deck"+i).GetComponent<Transform>().position.x,
	                                                    -4,
	                                                    GameObject.Find("Deck"+i).GetComponent<Transform>().position.z),2);
		deckCalled=true;
	}

	void onPlayerTurnChange(int i){
		iTween.MoveTo(GameObject.Find("Deck"+i), new Vector3(GameObject.Find("Deck"+i).GetComponent<Transform>().position.x,
		                                                     -7,
		                                                     GameObject.Find("Deck"+i).GetComponent<Transform>().position.z),2);
		deckCalled=false;
	}

	void OnGUI(){
		GUI.Label (new Rect(10, 10, 100, 20), "Player1 cards:"+GameObject.Find("Deck1").GetComponent<Deck>().cardNumber);
		GUI.Label (new Rect(10, 20, 100, 20), "Player2 cards:"+GameObject.Find("Deck2").GetComponent<Deck>().cardNumber);
		GUI.Label (new Rect(10, 30, 100, 20), "Player3 cards:"+GameObject.Find("Deck3").GetComponent<Deck>().cardNumber);
		GUI.Label (new Rect(10, 40, 100, 20), "Player4 cards:"+GameObject.Find("Deck4").GetComponent<Deck>().cardNumber);
		if(isGameOver){
			if(GUI.Button(new Rect(Screen.width/2-70,Screen.height/2-15,140,30),"Click to Play Again")){
				isGameOver=false;
				GameObject.Find("cardManager").GetComponent<CardManager>().ResetCard();
			}
		}

		if(playerTurn==1){
			if(!deckCalled){
				onPlayerTurn(playerTurn);
			}

			if(GUI.Button(new Rect(0,Screen.height-30,60,30),"P1 Turn")){
				GameObject.Find("cardChecker").GetComponent<CardChecker>().cardCalculation(selectedCard);
				if(deckCalled){
					onPlayerTurnChange(playerTurn);
				}
			}
			if(highestType!=""){
				if(GUI.Button(new Rect(70,Screen.height-30,60,30),"Skip")){
					ArrayUtility.Clear(ref selectedCard);
					if(deckCalled){
						onPlayerTurnChange(playerTurn);
					}
					ArrayUtility.Add(ref skipTurn,playerTurn);
					if(skipTurn.Length!=3){
						do{
							if((++playerTurn)==5){
								playerTurn=1;
							}
						}while(ArrayUtility.Contains(skipTurn,playerTurn));
					}else{
						playerTurn = highestPlayer;
						for(int i=0;i<selectedCardOnTable.Length;i++){
							selectedCardOnTable[i].SetActive(false);
						}
					}
				}
			}
		}else if(playerTurn==2){
			if(!deckCalled){
				onPlayerTurn(playerTurn);
			}
			if(GUI.Button(new Rect(0,Screen.height-30,60,30),"P2 Turn")){
				GameObject.Find("cardChecker").GetComponent<CardChecker>().cardCalculation(selectedCard);
				if(deckCalled){
					onPlayerTurnChange(playerTurn);
				}
			}
			if(highestType!=""){
				if(GUI.Button(new Rect(70,Screen.height-30,60,30),"Skip")){
					if(deckCalled){
						onPlayerTurnChange(playerTurn);
					}
					ArrayUtility.Add(ref skipTurn,playerTurn);
					ArrayUtility.Clear(ref selectedCard);
					if(skipTurn.Length!=3){
						do{
							if((++playerTurn)==5){
								playerTurn=1;
							}
						}while(ArrayUtility.Contains(skipTurn,playerTurn));
					}else{
						playerTurn = highestPlayer;
						for(int i=0;i<selectedCardOnTable.Length;i++){
							selectedCardOnTable[i].SetActive(false);
						}
					}
				}
			}
		}else if(playerTurn==3){
			if(!deckCalled){
				onPlayerTurn(playerTurn);
			}
			if(GUI.Button(new Rect(0,Screen.height-30,60,30),"P3 Turn")){
				GameObject.Find("cardChecker").GetComponent<CardChecker>().cardCalculation(selectedCard);
				if(deckCalled){
					onPlayerTurnChange(playerTurn);
				}
			}
			if(highestType!=""){
				if(GUI.Button(new Rect(70,Screen.height-30,60,30),"Skip")){
					ArrayUtility.Clear(ref selectedCard);
					if(deckCalled){
						onPlayerTurnChange(playerTurn);
					}
					ArrayUtility.Add(ref skipTurn,playerTurn);
					if(skipTurn.Length!=3){
						do{
							if((++playerTurn)==5){
								playerTurn=1;
							}
						}while(ArrayUtility.Contains(skipTurn,playerTurn));
					}else{
						playerTurn = highestPlayer;
						for(int i=0;i<selectedCardOnTable.Length;i++){
							selectedCardOnTable[i].SetActive(false);
						}
					}
				}
			}
		}else if(playerTurn==4){
			if(!deckCalled){
				onPlayerTurn(playerTurn);
			}
			if(GUI.Button(new Rect(0,Screen.height-30,60,30),"P4 Turn")){
				GameObject.Find("cardChecker").GetComponent<CardChecker>().cardCalculation(selectedCard);
				if(deckCalled){
					onPlayerTurnChange(playerTurn);
				}
			}
			if(highestType!=""){
				if(GUI.Button(new Rect(70,Screen.height-30,60,30),"Skip")){
					ArrayUtility.Clear(ref selectedCard);
					if(deckCalled){
						onPlayerTurnChange(playerTurn);
					}
					ArrayUtility.Add(ref skipTurn,playerTurn);
					if(skipTurn.Length!=3){
						do{
							if((++playerTurn)==5){
								playerTurn=1;
							}
						}while(ArrayUtility.Contains(skipTurn,playerTurn));
					}else{
						playerTurn = highestPlayer;
						for(int i=0;i<selectedCardOnTable.Length;i++){
							selectedCardOnTable[i].SetActive(false);
						}
					}
				}
			}
		}
	}
}