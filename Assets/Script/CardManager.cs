﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using Enum = System.Enum;

public class CardManager : MonoBehaviour {

	//using enum to mark the suits
	enum SUIT{Diamond,Club,Hearts,Spades};
	enum RANK{Three,Four,Five,Six,Seven,Eight,Nine,Ten,Jack,Queen,King,Ace,Two}

	public GameObject cardPrefab;
	public GameObject[] playerDeck;
	public GameObject[] cards;
	private Sprite[] cardSprite;

	private GameObject[] spriteSheet;

	void Awake(){
		cardSprite = Resources.LoadAll<Sprite>("CardsSprite");
		cards = new GameObject[52];
		
		int spriteIndex = 0;

		//all card instantiate section
		foreach(SUIT suit in Enum.GetValues(typeof(SUIT))){
			foreach(RANK rank in Enum.GetValues(typeof(RANK))){
				cards[spriteIndex] = Instantiate(cardPrefab,new Vector3(0,0,0),Quaternion.identity) as GameObject;
				cards[spriteIndex].GetComponent<Card>().cardIndex = spriteIndex;
				cards[spriteIndex].GetComponent<Card>().suit = (int)suit+1;
				cards[spriteIndex].GetComponent<Card>().rank = (int)rank+3;
				cards[spriteIndex].GetComponent<SpriteRenderer>().sprite =  cardSprite[spriteIndex];
				spriteIndex++;
			}
		}

		CardShuffle();
	}

	public void ResetCard(){
		//reset all card for new game
		for(int i=0;i<cards.Length;i++){
			cards[i].SetActive(true);
			cards[i].transform.parent=null;
		}
		GameObject.Find("Deck1").GetComponent<Deck>().cardNumber=0;
		GameObject.Find("Deck2").GetComponent<Deck>().cardNumber=0;
		GameObject.Find("Deck3").GetComponent<Deck>().cardNumber=0;
		GameObject.Find("Deck4").GetComponent<Deck>().cardNumber=0;
		GameObject.Find("Deck1").transform.position=new Vector3(0,-7,0);
		GameObject.Find("Deck2").transform.position=new Vector3(0,-7,0);
		GameObject.Find("Deck3").transform.position=new Vector3(0,-7,0);
		GameObject.Find("Deck4").transform.position=new Vector3(0,-7,0);
		ArrayUtility.Clear(ref GameObject.Find("gameManager").GetComponent<GameManager>().selectedCardOnTable);
		GameObject.Find("gameManager").GetComponent<GameManager>().deckCalled=false;
		CardShuffle();
	}

	void CardShuffle(){
		GameObject temp;

		for(int i=cards.Length-1; i>0; i--){
			int n = Random.Range(0,i+1);
			temp = cards[i];
			cards[i] = cards[n];
			cards[n] = temp;
		}

		//assign card into each player deck
		for(int i=0;i<13;i++){
			cards[i].GetComponent<Card>().inHandIndex = 1;
			cards[i].GetComponent<Card>().transform.position = new Vector3((-5+i),-7,0);
			cards[i].GetComponent<Card>().GetComponent<SpriteRenderer>().sortingOrder = i+1;
			cards[i].transform.parent = GameObject.Find("Deck1").transform;
			GameObject.Find("Deck1").GetComponent<Deck>().cardNumber++;
		}
		
		for(int i=13;i<26;i++){
			cards[i].GetComponent<Card>().inHandIndex = 2;
			cards[i].GetComponent<Card>().transform.eulerAngles = new Vector3(0,20,0);
			cards[i].GetComponent<Card>().transform.position = new Vector3((-5+(i-13)),-7,0);
			cards[i].GetComponent<Card>().GetComponent<SpriteRenderer>().sortingOrder = i+1;
			cards[i].transform.parent = GameObject.Find("Deck2").transform;
			GameObject.Find("Deck2").GetComponent<Deck>().cardNumber++;
		}
		
		for(int i=26;i<39;i++){
			cards[i].GetComponent<Card>().inHandIndex = 3;
			cards[i].GetComponent<Card>().transform.position = new Vector3((-5+(i-26)),-7,0);
			cards[i].GetComponent<Card>().GetComponent<SpriteRenderer>().sortingOrder = i+1;
			cards[i].transform.parent = GameObject.Find("Deck3").transform;
			GameObject.Find("Deck3").GetComponent<Deck>().cardNumber++;
		}
		
		for(int i=39;i<52;i++){
			cards[i].GetComponent<Card>().inHandIndex = 4;
			cards[i].GetComponent<Card>().transform.position = new Vector3((-5+(i-39)),-7,0);
			cards[i].GetComponent<Card>().GetComponent<SpriteRenderer>().sortingOrder = i+1;
			cards[i].transform.parent = GameObject.Find("Deck4").transform;
			GameObject.Find("Deck4").GetComponent<Deck>().cardNumber++;
		}

	}
}
