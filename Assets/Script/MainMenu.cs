﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {

	private int buttonWidth = 90;
	private int buttonHeight = 30;

	void OnGUI(){
		if(GUI.Button(new Rect((Screen.width/2)-(buttonWidth/2),(Screen.height/2)-(buttonHeight/2),buttonWidth,buttonHeight),"Start")){
			Application.LoadLevel(1);
		}

		if(GUI.Button(new Rect((Screen.width/2)-(buttonWidth/2),(Screen.height/2)-(buttonHeight/2)+(buttonHeight*2),buttonWidth,buttonHeight),"Quit")){
			Application.Quit();
		}
	}
}
